<?php
use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class ContactTest extends KernelTestCase
{
    public function getEntity(): Contact
    {
        return (new Contact())
            ->setFirstname('Alexis')
            ->setLastname('Theveny')
            ->setEmail('alexis.theveny@livecampus.tech')
            ->setPhone('0625628184');
    }

    /**
     * @throws Exception
     */
    public function testValidContact() {
        self::bootKernel();
        $error = self::getContainer()->get('validator')->validate($this->getEntity());
        $this->assertCount(0, $error);
    }

    /**
     * @throws Exception
     */
    public function testInvalidContact() {
        // Erverything wrong
        $invalidContact = $this->getEntity()
            ->setLastname("Alexis11")
            ->setFirstname("Theveny@")
            ->setEmail("email@")
            ->setPhone("abcdefghij");
        // Erverything blank
        $blankContact = $this->getEntity()
            ->setLastname("")
            ->setFirstname("")
            ->setEmail("")
            ->setPhone("");

        self::bootKernel();
        $error1 = self::getContainer()->get('validator')->validate($invalidContact);
        $error2 = self::getContainer()->get('validator')->validate($blankContact);
        $errorCount = sizeof($error1) + sizeof($error2);
        $this->assertNotEquals(0, $errorCount);
    }
    /**
     * @throws Exception
     */
    public function testInvalidContactFirstname() {
        self::bootKernel();
        // Invalid Firstname
        $error1 = self::getContainer()->get('validator')->validate($this->getEntity()->setFirstname("Alexis11"));
        // Blank Firstname
        $error2 = self::getContainer()->get('validator')->validate($this->getEntity()->setFirstname(""));
        $errorCount = sizeof($error1) + sizeof($error2);
        $this->assertNotEquals(0, $errorCount);
    }
    /**
     * @throws Exception
     */
    public function testInvalidContactLastname() {
        self::bootKernel();
        // Invalid Lastname
        $error1 = self::getContainer()->get('validator')->validate($this->getEntity()->setLastname("Theveny@"));
        // Blank Lastname
        $error2 = self::getContainer()->get('validator')->validate($this->getEntity()->setLastname(""));
        $errorCount = sizeof($error1) + sizeof($error2);
        $this->assertNotEquals(0, $errorCount);
    }
    /**
     * @throws Exception
     */
    public function testInvalidContactEmail() {
        self::bootKernel();
        // Invalid Email
        $error1 = self::getContainer()->get('validator')->validate($this->getEntity()->setEmail("alexis@gmail"));
        // Blank Email
        $error2 = self::getContainer()->get('validator')->validate($this->getEntity()->setEmail(""));
        $errorCount = sizeof($error1) + sizeof($error2);
        $this->assertNotEquals(0, $errorCount);
    }
    /**
     * @throws Exception
     */
    public function testInvalidContactPhone() {
        self::bootKernel();
        // Invalid Phone
        $error = self::getContainer()->get('validator')->validate($this->getEntity()->setPhone("abcdefghij"));
        $this->assertNotEquals(0, sizeof($error));
    }
}
?>