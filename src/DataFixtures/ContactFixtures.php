<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ContactFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
         $contact = new Contact();
         $contact->setFirstname("Alexis");
         $contact->setLastname("Theveny");
         $contact->setEmail("alexis.theveny@livecampus.tech");
         $contact->setPhone("0625628184");
         $manager->persist($contact);

        $manager->flush();
    }
}
